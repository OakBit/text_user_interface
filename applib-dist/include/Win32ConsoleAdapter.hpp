#pragma once
#include <Windows.h>
#undef min

#include <algorithm>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>

#include <ConsoleState.hpp>

#if defined(_DEBUG) && defined(_DLL)
#pragma comment (lib,"Win32ConsoleAdapterLib-mt-gd.lib")
#elif !defined(_DEBUG) && defined(_DLL)
#pragma comment (lib,"Win32ConsoleAdapterLib-mt.lib")
#endif
using namespace std;

class XError {
public:
	using id_type = decltype(GetLastError());
	using file_type = char const*;
	using string_type = std::string;
private:
	id_type code_;
	int line_;
	file_type file_;
public:
	XError(int line, file_type file);
	auto code() const->id_type;
	auto file() const->file_type;
	auto line() const -> int;

	string_type msg() const;
};

const enum Colour {
	BLACK_FORE = 0,
	DARKBLUE_FORE = FOREGROUND_BLUE,
	DARKGREEN_FORE = FOREGROUND_GREEN,
	DARKCYAN_FORE = FOREGROUND_GREEN | FOREGROUND_BLUE,
	DARKRED_FORE = FOREGROUND_RED,
	DARKMAGENTA_FORE = FOREGROUND_RED | FOREGROUND_BLUE,
	DARKYELLOW_FORE = FOREGROUND_RED | FOREGROUND_GREEN,
	DARKGRAY_FORE = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE,
	GRAY_FORE = FOREGROUND_INTENSITY,
	BLUE_FORE = FOREGROUND_INTENSITY | FOREGROUND_BLUE,
	GREEN_FORE = FOREGROUND_INTENSITY | FOREGROUND_GREEN,
	CYAN_FORE = FOREGROUND_INTENSITY | FOREGROUND_GREEN | FOREGROUND_BLUE,
	RED_FORE = FOREGROUND_INTENSITY | FOREGROUND_RED,
	MAGENTA_FORE = FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_BLUE,
	YELLOW_FORE = FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_GREEN,
	WHITE_FORE = FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE,

	BLACK_BACK = 0,
	DARKBLUE_BACK = BACKGROUND_BLUE,
	DARKGREEN_BACK = BACKGROUND_GREEN,
	DARKCYAN_BACK = BACKGROUND_GREEN | FOREGROUND_BLUE,
	DARKRED_BACK = BACKGROUND_RED,
	DARKMAGENTA_BACK = BACKGROUND_RED | BACKGROUND_BLUE,
	DARKYELLOW_BACK = BACKGROUND_RED | BACKGROUND_GREEN,
	DARKGRAY_BACK = BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE,
	GRAY_BACK = BACKGROUND_INTENSITY,
	BLUE_BACK = BACKGROUND_INTENSITY | BACKGROUND_BLUE,
	GREEN_BACK = BACKGROUND_INTENSITY | BACKGROUND_GREEN,
	CYAN_BACK = BACKGROUND_INTENSITY | BACKGROUND_GREEN | BACKGROUND_BLUE,
	RED_BACK = BACKGROUND_INTENSITY | BACKGROUND_RED,
	MAGENTA_BACK = BACKGROUND_INTENSITY | BACKGROUND_RED | BACKGROUND_BLUE,
	YELLOW_BACK = BACKGROUND_INTENSITY | BACKGROUND_RED | BACKGROUND_GREEN,
	WHITE_BACK = BACKGROUND_INTENSITY | BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE
};

class ConAdapt {
	/*===========================================================================
	*	Variables
	============================================================================*/

	HANDLE hConsoleInput, hConsoleOutput;
	CONSOLE_SCREEN_BUFFER_INFO	CSBI;
	CONSOLE_CURSOR_INFO			CCI;
	DWORD						ConsoleMode;
	short						CurrentConsoleWidth;

	/*===========================================================================
	*	Constructors
	============================================================================*/
public:
	ConAdapt();

	~ConAdapt();


	ConsoleState GetConsoleState();

	void SetConsoleState(ConsoleState const& state);

	//Use the ForegroundColour and BackgroundColour enums to pick a colour
	void PaintArea(short x, short y, short width, short height, WORD  colour);

	void HideCursor(bool isShown);

	void SetCursorPos(short x, short y);

	void EnableMouseInput(bool isEnabled);

	void ResizeWindow(short width, short height);

	void SetTitle(string title);

	void SetControlHandler(PHANDLER_ROUTINE const& phr);

	//Writing string will overwrite background colours so they need to be sent in with the component they are writing overtop
	void DrawString(short x, short y, string const& str, WORD colour);
	void DrawString(short x, short y, string const& str, vector<WORD> const& colours);
	//Writes a string at the cursor location, will wrap to the window width
	void WriteString(string const& str);

	//Will add any events to the passed vector and return numevents
	int GetEvents(vector<INPUT_RECORD>& EventRecords);

private:
	void GetCSBI();
	void GetCCI();
	void GetMode();
	void SetMode();
};