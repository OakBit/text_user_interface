#pragma once
#include <Windows.h>
#undef min

#include <algorithm>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <vector>
using namespace std;

class ConsoleState {
public:
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	vector<char> title;
	vector<CHAR_INFO> output;
	CONSOLE_CURSOR_INFO cursorInfo;
	DWORD mode;
};