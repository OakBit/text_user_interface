#pragma once
#include <set>
#include <Win32ConsoleAdapter.hpp>

class KeyHandler {
public:
	virtual void Pressed(KEY_EVENT_RECORD e) = 0;
};


class KeyEvent {
	set<KeyHandler*> handlers;
public:
	void addKeyHandler(KeyHandler* p) { handlers.insert(p); }
	void removeKeyHandler(KeyHandler* p) { handlers.erase(p); }
	void notifyKeyPressed(KEY_EVENT_RECORD e) {
		for (auto handler : handlers)
			handler->Pressed(e);
	}
};

