#pragma once
#include "ClickableComponent.hpp"

class Button :public ClickableComponent {
	string text;
public:
	Button(shared_ptr<ConAdapt> c, string t, short x, short y, short width, short height, short colour);
	string Text(){
		return text;
	}
	void Text(string t){
		text = t;
		Refresh();
	}

	virtual void Clicked() override {}

	void Refresh() override;


};