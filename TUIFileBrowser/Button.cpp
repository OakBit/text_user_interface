#include "Button.hpp"

Button::Button(shared_ptr<ConAdapt> c, string t, short x, short y, short width, short height, short colour) :text(t), ClickableComponent(c, x, y, width, height, colour) { 
	pConsole->DrawString(x + (width / 2 - text.length() / 2), y + (height / 2), text, colour); 
}

void Button::Refresh() {
	ClickableComponent::Refresh();
	//Draw a centered string in the component
	pConsole->DrawString(x + (width / 2 - text.length() / 2), y + (height / 2), text, colour);
}