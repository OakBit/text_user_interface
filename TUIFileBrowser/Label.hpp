#pragma once
//Draws text on screen with the background attributes that are already there
#include "ViewableComponent.hpp"


class Label {
	short x, y;
	short colour;
	string text;
	shared_ptr<ConAdapt> pConsole;
public:
	//ClickableComponent(shared_ptr<ConAdapt> c, short x, short y, short width, short height, short colour) :ViewableComponent(c, x, y, width, height, colour) {}

	Label(shared_ptr<ConAdapt>  c, short x, short y, short colour, string const& t);

	string Text();
	void Text(string t);

	void Refresh();

};
