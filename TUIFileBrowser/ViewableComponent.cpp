
#include "ViewableComponent.hpp"

ViewableComponent::ViewableComponent(shared_ptr<ConAdapt> c, short x, short y, short width, short height, short colour) {
	this->x = x;
	this->y = y;
	this->width = width;
	this->height = height;
	this->colour = colour;
	this->pConsole = c;

	Refresh();
}

bool ViewableComponent::isWithinBounds(short x, short y, short width , short height ) {
	if (x >= this->x && y >= this->y && width + x < this->width + this->x && height + y < this->height + this->y)
		return true;
	else
		return false;
}


//Will redraw
void ViewableComponent::Refresh() {
	pConsole->PaintArea(x, y, width, height, colour);
}