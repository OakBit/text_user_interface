#include "Label.hpp"

Label::Label(shared_ptr<ConAdapt>  c, short x, short y, short colour, string const& t) :text(t), x(x), y(y), pConsole(c), colour(colour) { 
	pConsole->DrawString(x, y, text, colour); 
}

string Label::Text() {
	return text;
}
void Label::Text(string t) {
	text = t;
	Refresh();
}

void Label::Refresh() {
	pConsole->DrawString(x, y, text, colour);
}
