#pragma once
#include "KeyEvent.hpp"
#include "ClickableComponent.hpp"

class ListBox :public ClickableComponent, public KeyEvent {
	bool isFocused;
	vector<string> textLines;
	short cursorPos = 0;
	short editControlAperture = 0;
public:
	ListBox(shared_ptr<ConAdapt> c, short x, short y, short width, short height, short colour);


	bool Focused() {
		return isFocused;
	}
	//Cursor should be 1 after the last letter
	void Focused(bool f, short pos = -1);

	short CursorPos() { return cursorPos; }
	void CursorPos(short p) {
		if (p > -1 && p < textLines.size())
		{
			cursorPos = p;
		}
	}

	void addLine(string line);
	void removeLineAt(int index);

	size_t numLines() { return textLines.size(); }

	void Clear() { textLines.clear(); Refresh(); }

	virtual void Clicked() override {}

	void Refresh() override;


};