#include <iostream>
#include <map>
#include <App.hpp>
#include <Win32ConsoleAdapter.hpp>

#include "ViewableComponent.hpp"
#include "ClickableComponent.hpp"
#include "Label.hpp"
#include "CheckBox.hpp"
#include "Button.hpp"
#include "ListBox.hpp"
#include "TextBox.hpp"

#include <regex>
#include <filesystem>
using namespace std::tr2::sys;
using namespace std;

/*
THIS WILL BE VIEW LOGIC, it should handle user inputs, for now we have only 1 view
*/

// Application data
bool applicationQuitting = false;
DWORD terminationEventIdx = -1;
bool recurse = false;
string scanPath;
string filterExpression = "";
bool keyPressed = false;

regex filterRegex;

short WINDOW_WIDTH = 120;
short WINDOW_HEIGHT = 30;

//Store different components in seperate containers, and notify it with the events it needs based on what component it is
map<string, CheckBox> CheckBoxes;
map<string, Label> Labels;
map<string, ViewableComponent> DrawnAreas;
map<string, Button> Buttons;
map<string, ListBox> ListBoxes;
map<string, TextBox> TextBoxes;


// Control Event Handler, for graceful close
BOOL CtrlHandler(DWORD ctrlType) {
	if (ctrlType <= CTRL_CLOSE_EVENT) {
		terminationEventIdx = ctrlType;
		applicationQuitting = true;
		return TRUE;
	}
	return FALSE;
}
void scan(path const& f, unsigned i = 0)
{
	string indent(i, '   ');

	directory_iterator d(f);  // creates a directory iterator
							  // that points to the dir f
	/*{
		ListBoxes.at("lbFiles").addLine(indent + f.filename().string());
		ListBoxes.at("lbFiles").CursorPos(ListBoxes.at("lbFiles").CursorPos() + 1);
	}*/
	directory_iterator e;     // end of any folder
	for (; d != e; ++d)
	{
		if (std::regex_match(d->path().filename().string(), filterRegex))
		{
			if (d->path().has_filename())
			{
				ListBoxes.at("lbFiles").addLine(indent + d->path().filename().string());
				ListBoxes.at("lbFiles").CursorPos(ListBoxes.at("lbFiles").CursorPos() + 1);
			}
		}
		//cout << indent << d->path() <<
		//	(is_directory(d->status()) ? " [dir]" : "")
		//	<< " ext = " << d->path().extension() << endl;
		// if the item is a directory use recursion to parse that
		if (is_directory(d->status()) && recurse)
			scan(d->path(), i + 1);
	}
}

void ScanDirs() {
	scanPath = TextBoxes.at("tbFolder").Text();
	filterExpression = TextBoxes.at("tbFilter").Text();
	regex r;
	try {
		r = regex(filterExpression);
	}
	catch (...) {
		Labels.at("lblStatus").Text("Invalid regex supplied for filter.");
		return;
	}

	filterRegex.assign(r);

	if (exists(scanPath))
	{
		ListBoxes.at("lbFiles").Clear();
		Labels.at("lblStatus").Text("Scanning " + scanPath);

		scan(scanPath);

		Labels.at("lblStatus").Text("Done Scanning  " + scanPath);
	}
	else
		Labels.at("lblStatus").Text("Invalid path -> " + scanPath);
}

void Close() {
	applicationQuitting = true;

}
/*=========================================================================================
MAIN LOGIC
=========================================================================================*/
class FileSystemTUI: App, MouseHandler
{
	int execute() override {
		#pragma region Setup
		//Create our instance of the console adapter
		std::shared_ptr<ConAdapt> pConsole = std::make_shared<ConAdapt>();
		//save the previous state so we can reset our changes
		ConsoleState previousConsoleState = pConsole->GetConsoleState();

		pConsole->SetControlHandler((PHANDLER_ROUTINE)CtrlHandler);

		pConsole->ResizeWindow(WINDOW_WIDTH, WINDOW_HEIGHT);
		pConsole->HideCursor(false);

		pConsole->EnableMouseInput(true);
		#pragma endregion

		/*=============================================================
		TUI component handlers
		=============================================================*/
		#pragma region Listeners

		//Handler definition for a clickable component
		class CheckBoxClickListener :public MouseEvent {
			CheckBox& c;
		public :
			CheckBoxClickListener(CheckBox& clickComp) : c(clickComp) {}
			void Clicked(MOUSE_EVENT_RECORD e) override {

				if (c.isWithinBounds(e.dwMousePosition.X, e.dwMousePosition.Y))
					if (e.dwButtonState & FROM_LEFT_1ST_BUTTON_PRESSED)
					{
						c.Checked(!c.Checked());
						recurse = c.Checked();
						c.Refresh();
					}
			}
		};

		class ScanButtonClickListener :public MouseEvent {
			Button& c;
		public:
			ScanButtonClickListener(Button& clickComp) : c(clickComp) {}
			void Clicked(MOUSE_EVENT_RECORD e) override {
				if (c.isWithinBounds(e.dwMousePosition.X, e.dwMousePosition.Y))
					if (e.dwButtonState & FROM_LEFT_1ST_BUTTON_PRESSED)
					{
						ScanDirs();
						c.Refresh();
					}
			}
		};

		class ExitButtonClickListener :public MouseEvent {
			Button& c;
		public:
			ExitButtonClickListener(Button& clickComp) : c(clickComp) {}
			void Clicked(MOUSE_EVENT_RECORD e) override {
				if (c.isWithinBounds(e.dwMousePosition.X, e.dwMousePosition.Y))
					if (e.dwButtonState & FROM_LEFT_1ST_BUTTON_PRESSED)
					{
						Close();
						c.Refresh();
					}
			}
		};

		class TextBoxClickListener :public MouseEvent {
			TextBox& c;
		public:
			TextBoxClickListener(TextBox& clickComp) : c(clickComp) {}
			void Clicked(MOUSE_EVENT_RECORD e) override {
				bool withinBound = c.isWithinBounds(e.dwMousePosition.X, e.dwMousePosition.Y);
				bool clicked = e.dwButtonState & FROM_LEFT_1ST_BUTTON_PRESSED;

				if (clicked)
				{
					if (withinBound)
					{
						c.Focused(true, e.dwMousePosition.X);
						c.Refresh();
					}
					else
					{
						//Necessary to refresh here?
						c.Focused(false);
					}
				}
			}
		};

		class ListBoxClickListener :public MouseEvent {
			ListBox& c;
		public:
			ListBoxClickListener(ListBox& clickComp) : c(clickComp) {}
			void Clicked(MOUSE_EVENT_RECORD e) override {
				bool withinBound = c.isWithinBounds(e.dwMousePosition.X, e.dwMousePosition.Y);
				bool clicked = e.dwButtonState & FROM_LEFT_1ST_BUTTON_PRESSED;

				if (clicked)
				{
					if (withinBound)
					{
						c.Focused(true, e.dwMousePosition.Y);
						c.Refresh();
					}
					else
					{
						//Necessary to refresh here?
						c.Focused(false);
					}
				}
			}
		};

		//Handler definitions for components that listen to key input
		class ListBoxKeyListener : public KeyHandler {
			ListBox& lb;
		public:
			ListBoxKeyListener(ListBox& clickComp) : lb(clickComp) {}
			void Pressed(KEY_EVENT_RECORD e) override {
				if (e.bKeyDown && !keyPressed) {
					if (lb.Focused()) {
						keyPressed = true;

						switch (e.wVirtualKeyCode) {
						case VK_UP:
							lb.CursorPos( lb.CursorPos() - 1);
							lb.Refresh();
							break;
						case VK_PRIOR:
							lb.CursorPos( lb.CursorPos() - lb.height < 0 ? 0 : lb.CursorPos() - lb.height);
							lb.Refresh();
							break;
						case VK_NEXT:
							lb.CursorPos(lb.CursorPos() + lb.height > lb.numLines()-1 ? lb.numLines()-1 : lb.CursorPos() + lb.height );
							lb.Refresh();
							break;
						case VK_DOWN:
							lb.CursorPos(lb.CursorPos()+1);
							lb.Refresh();
							break;
						default:
							break;
						}
					}
				}
				else
				{
					keyPressed = false;
				}
			}
		};

		class TextBoxKeyListener : public KeyHandler {
			TextBox& tb;
		public:
			TextBoxKeyListener(TextBox& clickComp) : tb(clickComp) {}
			void Pressed(KEY_EVENT_RECORD e) override {
				if (e.bKeyDown && !keyPressed) {
					if (tb.Focused()) {
						keyPressed = true;

						switch (e.wVirtualKeyCode) {
						case VK_BACK:
							// backspace to remove at cursor location
							if (0 < tb.CursorPos() && tb.CursorPos() <= tb.Text().size()) {
								tb.CursorPos(tb.CursorPos() - 1);
								string s = tb.Text();
								s.erase(tb.CursorPos(), 1);
								tb.Text(s);
							}
							break;

						case VK_DELETE:
							if (0 <= tb.CursorPos() && tb.CursorPos() < tb.Text().size())
								tb.Text(tb.Text().erase(tb.CursorPos(), 1));
							break;

						case VK_LEFT:
							if (tb.CursorPos() > 0)
								tb.CursorPos(tb.CursorPos() - 1);
							break;

						case VK_RIGHT:
							if (tb.CursorPos() < tb.Text().size())
								tb.CursorPos(tb.CursorPos() + 1);
							break;

						case VK_END:
							tb.CursorPos(tb.Text().size());
							break;

						case VK_HOME:
							tb.CursorPos(0);
							break;

						case VK_RETURN:
							//HERE WE WANT TO START SCANNING AND SAY THAT IN THE STATUS
							//OutputString(2, TITLE_SECTION_START + 1, editControlString, TITLE_SECTION_ATTR);
							break;

						default:
							// add character
							char ch = e.uChar.AsciiChar;
							if (isprint(ch))
							{
								string s = tb.Text();
								s.insert(tb.CursorPos() + s.begin(), ch);
								tb.Text(s);
								tb.CursorPos(tb.CursorPos() + 1);
							}
						}
					}
				}
				else
				{
					keyPressed = false;
				}
			}
		};

		#pragma endregion

		/*=============================================================
		Layout TUI
		=============================================================*/
		#pragma region Layout and Register Components
		//TOP BAR
		ViewableComponent viewBarTop(pConsole, 0, 0, WINDOW_WIDTH, 5, Colour::DARKBLUE_BACK);

		//FOLDER
		string folderLabelTxt = "Root Folder : ";
		Label lblFolder(pConsole, 3, 2, Colour::DARKBLUE_BACK | Colour::WHITE_FORE, folderLabelTxt);
		TextBoxes.insert(pair<string, TextBox>("tbFolder", TextBox (pConsole, "", 3 + folderLabelTxt.size(), 2, 15, Colour::GRAY_BACK | Colour::BLACK_FORE )));
		TextBoxClickListener textBoxFolderListener(TextBoxes.at("tbFolder"));
		TextBoxes.at("tbFolder").addHandler(&textBoxFolderListener);
		TextBoxKeyListener textBoxFolderKeyListener(TextBoxes.at("tbFolder"));
		TextBoxes.at("tbFolder").addKeyHandler(&textBoxFolderKeyListener);

		//FILTER
		string filterLabelTxt = "Filter : ";
		Label lblFilter(pConsole, 35, 2, Colour::DARKBLUE_BACK | Colour::WHITE_FORE, filterLabelTxt);
		TextBoxes.insert(pair<string, TextBox>("tbFilter", TextBox(pConsole, "", 35 + filterLabelTxt.size(), 2, 10, Colour::GRAY_BACK | Colour::BLACK_FORE)));
		TextBoxClickListener textBoxFilterListener(TextBoxes.at("tbFilter"));
		TextBoxes.at("tbFilter").addHandler(&textBoxFilterListener);
		TextBoxKeyListener textBoxFilterKeyListener(TextBoxes.at("tbFilter"));
		TextBoxes.at("tbFilter").addKeyHandler(&textBoxFilterKeyListener);

		//RECURSE
		string recurseLabelTxt = "Recurse Directories : ";
		Label lblRecurse(pConsole, WINDOW_WIDTH - recurseLabelTxt.size() - 39, 2, Colour::DARKBLUE_BACK | Colour::WHITE_FORE, recurseLabelTxt);
		
		CheckBoxes.insert(pair<string, CheckBox>("chkRecurse", CheckBox(pConsole, WINDOW_WIDTH - 38 ,2,Colour::GRAY_BACK | Colour::WHITE_FORE)));
		CheckBoxClickListener checkBoxListener(CheckBoxes.at("chkRecurse"));
		CheckBoxes.at("chkRecurse").addHandler(&checkBoxListener);

		//SCAN
		Buttons.insert(pair<string, Button>("btnScan", Button(pConsole, "SCAN", WINDOW_WIDTH-32, 1, 15, 3, Colour::BLACK_BACK | Colour::WHITE_FORE)));
		ScanButtonClickListener scanListener(Buttons.at("btnScan"));
		Buttons.at("btnScan").addHandler(&scanListener);

		//EXIT
		Buttons.insert(pair<string, Button>("btnExit", Button(pConsole, "EXIT", WINDOW_WIDTH - 16, 1, 15, 3, Colour::BLACK_BACK | Colour::WHITE_FORE)));
		ExitButtonClickListener buttonListener(Buttons.at("btnExit"));
		Buttons.at("btnExit").addHandler(&buttonListener);

		//MIDDLE SECTION
		ListBoxes.insert(pair<string, ListBox>("lbFiles", ListBox(pConsole, 0, 5, WINDOW_WIDTH, WINDOW_HEIGHT-10, Colour::BLACK_BACK | Colour::WHITE_FORE)));
		ListBoxClickListener listBoxListener(ListBoxes.at("lbFiles"));
		ListBoxKeyListener listBoxKeyListener(ListBoxes.at("lbFiles"));
		ListBoxes.at("lbFiles").addHandler(&listBoxListener);
		ListBoxes.at("lbFiles").addKeyHandler(&listBoxKeyListener);

		//BOTTOM BAR
		ViewableComponent viewBarBottom(pConsole, 0, WINDOW_HEIGHT - 5, WINDOW_WIDTH, 5, Colour::DARKBLUE_BACK);

		//STATUS
		string statusLabelTxt = "Status ";
		Labels.insert(pair<string,Label>("lblStatus",Label (pConsole, 1, WINDOW_HEIGHT-4, Colour::DARKBLUE_BACK | Colour::WHITE_FORE, statusLabelTxt)));

		//TOTALS
		string foldersLabelTxt = "Folders : ";
		Labels.insert(pair<string, Label>("lblFolders", Label (pConsole, WINDOW_WIDTH / 3 * 0+1, WINDOW_HEIGHT - 3, Colour::DARKBLUE_BACK | Colour::WHITE_FORE, foldersLabelTxt)));
		string filesMatchedLabelTxt = "Files Matched : 0";
		Labels.insert(pair<string, Label>("lblFilesMatched", Label (pConsole, WINDOW_WIDTH / 3 * 1+1, WINDOW_HEIGHT - 3, Colour::DARKBLUE_BACK | Colour::WHITE_FORE, filesMatchedLabelTxt)));
		string fileSizeLabelTxt = "Matched Files Size : 0";
		Labels.insert(pair<string, Label>("lblMatchedFileSize", Label (pConsole, WINDOW_WIDTH / 3 * 2+1, WINDOW_HEIGHT - 3, Colour::DARKBLUE_BACK | Colour::WHITE_FORE, fileSizeLabelTxt)));

		//SELECTED STATS
		string selectedItemStatsTxt = "Selected Item Stats :: ";
		Label lblSelectedItem(pConsole, 1, WINDOW_HEIGHT - 2, Colour::DARKBLUE_BACK | Colour::WHITE_FORE, selectedItemStatsTxt);

		string sizeTxt = "Size : ";
		Labels.insert(pair<string, Label>("lblSelectedSize", Label (pConsole, selectedItemStatsTxt.size() + 3, WINDOW_HEIGHT - 2, Colour::DARKBLUE_BACK | Colour::WHITE_FORE, sizeTxt)));
		string pathTxt = "Path : ";
		Labels.insert(pair<string, Label>("lblSelectedPath", Label (pConsole, selectedItemStatsTxt.size() + 3 + sizeTxt.size() + 15, WINDOW_HEIGHT - 2, Colour::DARKBLUE_BACK | Colour::WHITE_FORE, pathTxt)));

		#pragma endregion

		/*
		LOAD ARGS FROM CONSOLE STARTUP
		*/
		for (auto arg : GetArgs())
		{
			if (arg == "-r")
			{
				CheckBoxes.at("chkRecurse").Checked(true);
				recurse = true;
			}
			else {
				path p = arg;
				if (exists(p))
					TextBoxes.at("tbFolder").Text(arg);
				else
					TextBoxes.at("tbFilter").Text(arg);
			}
		}


		/*=============================================================
		Handle input
		=============================================================*/
		#pragma region Input Logic
		//For now we only want to update on keystrokes and mouse clicks and release
		vector<INPUT_RECORD> events(128);
		while (!applicationQuitting) {
			int numEvents;
			numEvents = pConsole->GetEvents(events);

			for (decltype(numEvents) i = 0; i < numEvents; ++i) {
				auto& e = events[i];
				switch (e.EventType) {
				case FOCUS_EVENT: break;
				case KEY_EVENT:							
					for (pair<string, TextBox> click : TextBoxes) {
						click.second.notifyKeyPressed(e.Event.KeyEvent);
					}
					for (pair<string, ListBox> click : ListBoxes) {
						click.second.notifyKeyPressed(e.Event.KeyEvent);
					}
					
					break;
					//for (pair<string, ListBox> click : ListBoxes) {
					//	click.second.notify(e.Event.MouseHandler);
					//}break;
				case MOUSE_EVENT:	
					//SEND THE MOUSEEVENT TO EVERY COMPONENT AND IT DECIDES WHAT TO DO WITH IT

					for (pair<string, CheckBox> click : CheckBoxes) {
						click.second.notify(e.Event.MouseEvent);
					}
					for (pair<string, Button> click : Buttons) {
						click.second.notify(e.Event.MouseEvent);
					}
					for (pair<string, TextBox> click : TextBoxes) {
						click.second.notify(e.Event.MouseEvent);
					}
					for (pair<string, ListBox> click : ListBoxes) {
						click.second.notify(e.Event.MouseEvent);
					}
					break;//ProcessMouseHandler(e.Event.MouseHandler); break;
				}
			}
		}

		#pragma endregion

		//Restore the state before we close
		pConsole->SetConsoleState(previousConsoleState);

		return EXIT_SUCCESS;
	}
}FileTUI;