#include "TextBox.hpp"

TextBox::TextBox(shared_ptr<ConAdapt> c, string t, short x, short y, short width, short colour) :isFocused(false), text(t), ClickableComponent(c, x, y, width, 1, colour) { Refresh(); }

void TextBox::Focused(bool f, short pos) {
	isFocused = f;

	CursorPos(text.size());
	if (Focused())
	{
		pConsole->HideCursor(true);
	}
	else {
		//pConsole->HideCursor(false);
	}
	//Refresh();
}
void TextBox::CursorPos(short p)
{
	if (p > -1)
	{
		cursorPos = p;
		Refresh();
	}
}

void TextBox::Refresh() {
	ClickableComponent::Refresh();

	auto practicalSize = text.size() + 1;
	while (cursorPos < editControlAperture)
		--editControlAperture;

	while (cursorPos - editControlAperture >= width)
		++editControlAperture;

	while (practicalSize - editControlAperture<width && practicalSize > width)
		--editControlAperture;

	auto s = text.substr(editControlAperture, width);
	s += string(width - s.size(), ' ');

	pConsole->DrawString(x, y, s, colour);

	short xLoc = x;
	xLoc += SHORT(cursorPos - editControlAperture);
	pConsole->SetCursorPos(xLoc, y);
}