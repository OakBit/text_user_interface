#pragma once
#include "KeyEvent.hpp"
#include "ClickableComponent.hpp"

class TextBox :public ClickableComponent, public KeyEvent {
	bool isFocused;
	string text;
	short cursorPos = 0;
	short editControlAperture = 0;
public:
	TextBox(shared_ptr<ConAdapt> c, string t, short x, short y, short width, short colour);

	bool Focused() {
		return isFocused;
	}
	//Cursor should be 1 after the last letter
	void Focused(bool f, short pos = -1);

	short CursorPos() { return cursorPos; }
	void CursorPos(short p);

	string Text() { return text; }
	void Text(string t) { 
		text = t; 
		Refresh(); }

	virtual void Clicked() override {}

	void Refresh() override;


};