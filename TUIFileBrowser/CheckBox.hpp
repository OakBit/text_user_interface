#pragma once
#include "ClickableComponent.hpp"

class CheckBox :public ClickableComponent {
private:
	bool isChecked;
public:
	CheckBox(shared_ptr<ConAdapt> c, short x, short y, short colour) :ClickableComponent(c, x, y, 1, 1, colour) {}

	bool Checked() { return isChecked; }
	void Checked(bool isChecked) { this->isChecked = isChecked; Refresh(); }

	virtual void Clicked() override {}

	void Refresh() override;


};