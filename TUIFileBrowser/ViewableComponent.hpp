#pragma once
#include <memory>
#include <Win32ConsoleAdapter.hpp>
//This component will be drawn on the console window
//It will contain its location and size information

class ViewableComponent {
public:
	short x;
	short y;
	short width;
	short height;
	short colour;

	shared_ptr<ConAdapt> pConsole;
	ViewableComponent(shared_ptr<ConAdapt> c, short x, short y, short width, short height, short colour);
	virtual ~ViewableComponent() {};

	short GetColour() { return colour; }
	void SetColour(short colour) { this->colour = colour; }

	bool isWithinBounds(short x, short y, short width=0, short height=0);
	//Will redraw
	virtual void Refresh();
};