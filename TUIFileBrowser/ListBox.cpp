#include "ListBox.hpp"

ListBox::ListBox(shared_ptr<ConAdapt> c, short x, short y, short width, short height, short colour) :isFocused(false), ClickableComponent(c, x, y, width, height, colour) { Refresh(); }

void ListBox::Focused(bool f, short pos) {
	isFocused = f;
	if (pos > y - 1 && pos < textLines.size() + y)
	{
		CursorPos(editControlAperture + pos-y);//Set the cursor position relative to the contents of the listbox
	}
	else
		CursorPos(0);
	if (Focused())
	{
		Refresh();
	}
}

void ListBox::addLine(string line) { textLines.push_back(line); ++editControlAperture; Refresh(); }

void ListBox::removeLineAt(int index) {
	if (index > -1 && index <= textLines.size())
	{
		textLines.erase(textLines.begin() + index);
		--editControlAperture;
		Refresh();
	}
}

void ListBox::Refresh() {
	ClickableComponent::Refresh();

	auto practicalSize = textLines.size() + 1;
	while (cursorPos < editControlAperture)
		--editControlAperture;

	while (cursorPos - editControlAperture >= height)
		++editControlAperture;

	while (practicalSize - editControlAperture < height && practicalSize > height)
		--editControlAperture;

	//s += string(width - s.size(), ' ');

	for (int i = editControlAperture; i < editControlAperture + height; ++i)
	{
		if (i < textLines.size())
		{
			if (i == cursorPos)
				pConsole->DrawString(x, y + i - editControlAperture, textLines[i], colour ^ BACKGROUND_INTENSITY ^ FOREGROUND_INTENSITY);
			else
				pConsole->DrawString(x, y + i - editControlAperture, textLines[i], colour);
		}
	}
}