#pragma once
#include <set>
#include "ViewableComponent.hpp"
//This will inherit from viewable component
//It will add some event handling logic from the console
//Will handle mouse press
//Will handle hover
//Will handle mouse release
//Inherits mouse events

//will have open logic handlers for all the events
class MouseEvent {
public:
	virtual void Clicked(MOUSE_EVENT_RECORD e) {};
};


class MouseHandler {
	set<MouseEvent*> handlers;
public:
	void addHandler(MouseEvent* p) { handlers.insert(p); }
	void removeHandler(MouseEvent* p) { handlers.erase(p); }
	void notify(MOUSE_EVENT_RECORD e) {
		for (auto handler : handlers)
			handler->Clicked(e);
	}
};


class ClickableComponent:public ViewableComponent, public MouseHandler {
public:
	ClickableComponent(shared_ptr<ConAdapt> c, short x, short y, short width, short height, short colour) :ViewableComponent(c,x,y,width,height,colour) {}
	virtual ~ClickableComponent() {}

	virtual void Clicked() = 0;

	void Refresh() override {
		ViewableComponent::Refresh();
	}


};

//
//class MouseClickHandler : public MouseObserver {
//public:
//	ClickableComponent const& cc;
//	MouseClickHandler(ClickableComponent const& component) :cc(component) {};
//};