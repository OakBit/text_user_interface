#include <Win32ConsoleAdapter.hpp>
#pragma region XError Class and Utilities
/* ErrorDescription converts Win32 error codes into a human readable string. */
string ErrorDescription(DWORD dwMessageID) {
	char* msg;
	auto c = FormatMessageA(
		/* flags */ FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS | FORMAT_MESSAGE_MAX_WIDTH_MASK,
		/* source */ NULL,
		/* message ID */ dwMessageID,
		/* language */		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		/* buffer */	(LPSTR)&msg,
		/* size */		0,
		/* args */		NULL);
	string strMsg = (c == 0)
		? "unknown"
		: msg;
	LocalFree(msg);
	return strMsg;
}


XError::id_type code_;
int line_;
XError::file_type file_;

XError::XError(int line, file_type file) :code_(GetLastError()), line_(line), file_(file) {}
auto XError::code() const -> id_type { return code_; }
auto XError::file() const -> file_type { return file_; }
auto XError::line() const -> int { return line_; }

XError::string_type XError::msg() const {
	ostringstream oss;
	oss << "Error: " << code() << "\n" << ErrorDescription(code()) << "\nIn: " << file() << "\nLine: " << line() << "\n";
	return oss.str();
}


#define THROW_CONSOLE_ERROR() throw XError(__LINE__, __FILE__)
#define THROW_IF_CONSOLE_ERROR(res) if(!res) throw XError(__LINE__, __FILE__)
#pragma endregion
#pragma region Console Adapter
ConAdapt::ConAdapt() {
	hConsoleOutput = GetStdHandle(STD_OUTPUT_HANDLE);
	hConsoleInput = GetStdHandle(STD_INPUT_HANDLE);
}

ConAdapt::~ConAdapt() {}


ConsoleState ConAdapt::GetConsoleState() {
	ConsoleState s;

	s.title = vector<char>(64 * 1024);
	s.title.resize(GetConsoleTitleA(s.title.data(), (DWORD)s.title.size()) + 1);
	s.title.shrink_to_fit();

	THROW_IF_CONSOLE_ERROR(GetConsoleScreenBufferInfo(hConsoleOutput, &s.csbi));

	SMALL_RECT bufferRect{ 0 };
	bufferRect.Right = s.csbi.dwSize.X - 1;
	bufferRect.Bottom = s.csbi.dwSize.Y - 1;
	s.output = vector<CHAR_INFO>(s.csbi.dwSize.X* s.csbi.dwSize.Y);
	THROW_IF_CONSOLE_ERROR(ReadConsoleOutputA(hConsoleOutput, s.output.data(), s.csbi.dwSize, COORD{ 0 }, &bufferRect));

	THROW_IF_CONSOLE_ERROR(GetConsoleCursorInfo(hConsoleOutput, &s.cursorInfo));

	GetConsoleMode(hConsoleInput, &s.mode);

	//When we get the state we should be aware of the settings in case changes are made
	this->CSBI = s.csbi;
	this->CCI = s.cursorInfo;
	this->ConsoleMode = s.mode;

	return s;
}

void ConAdapt::SetConsoleState(ConsoleState const& state) {
	//When we set the state we should be aware of the settings
	this->CSBI = state.csbi;
	this->CCI = state.cursorInfo;
	this->ConsoleMode = state.mode;

	THROW_IF_CONSOLE_ERROR(SetConsoleTitleA(state.title.data()));

	SMALL_RECT sr{ 0 };
	THROW_IF_CONSOLE_ERROR(SetConsoleWindowInfo(hConsoleOutput, TRUE, &sr));
	THROW_IF_CONSOLE_ERROR(SetConsoleScreenBufferSize(hConsoleOutput, state.csbi.dwSize));
	THROW_IF_CONSOLE_ERROR(SetConsoleWindowInfo(hConsoleOutput, TRUE, &state.csbi.srWindow));

	//SMALL_RECT bufferRect{ 0 };
	sr.Right = state.csbi.dwSize.X - 1;
	sr.Bottom = state.csbi.dwSize.Y - 1;
	THROW_IF_CONSOLE_ERROR(WriteConsoleOutputA(hConsoleOutput, state.output.data(), state.csbi.dwSize, COORD{ 0 }, &sr));

	THROW_IF_CONSOLE_ERROR(SetConsoleTitleA(state.title.data()));

	THROW_IF_CONSOLE_ERROR(SetConsoleTextAttribute(hConsoleOutput, state.csbi.wAttributes));

	THROW_IF_CONSOLE_ERROR(SetConsoleCursorInfo(hConsoleOutput, &state.cursorInfo));

	THROW_IF_CONSOLE_ERROR(SetConsoleCursorPosition(hConsoleOutput, state.csbi.dwCursorPosition));

	SetConsoleMode(hConsoleInput, state.mode);
}

void ConAdapt::SetControlHandler(PHANDLER_ROUTINE const& phr) {
	THROW_IF_CONSOLE_ERROR(SetConsoleCtrlHandler(phr, TRUE));
}

void ConAdapt::PaintArea(short x, short y, short width, short height, WORD  colour) {
	DWORD charsWritten;
	//Make sure that the dimensions given are within the console bounds
	width = min((SHORT)width, (SHORT)(this->CSBI.dwMaximumWindowSize.X - x));
	
	//Only draw until the Height we specify
	for (int i = y; i < y + height; ++i)
	{
		COORD startCoord{ x, i };
		THROW_IF_CONSOLE_ERROR(FillConsoleOutputCharacterA(hConsoleOutput, ' ', width, startCoord, &charsWritten));
		THROW_IF_CONSOLE_ERROR(FillConsoleOutputAttribute(hConsoleOutput, colour, width, startCoord, &charsWritten));
	}
}

void ConAdapt::HideCursor(bool isShown) {
	GetCCI();
	if (this->CCI.bVisible != isShown)
	{
		this->CCI.bVisible = isShown;
		THROW_IF_CONSOLE_ERROR(SetConsoleCursorInfo(hConsoleOutput, &this->CCI));
	}
}

void ConAdapt::SetCursorPos(short x, short y) {
	COORD loc{ x, y };
	THROW_IF_CONSOLE_ERROR(SetConsoleCursorPosition(hConsoleOutput, loc));
}


void ConAdapt::EnableMouseInput(bool isEnabled) {
	DWORD conMode = ENABLE_WINDOW_INPUT | ENABLE_PROCESSED_INPUT;
	if (isEnabled)
		conMode = conMode | ENABLE_EXTENDED_FLAGS | ENABLE_MOUSE_INPUT;
	THROW_IF_CONSOLE_ERROR(SetConsoleMode(hConsoleInput, conMode));
}

void ConAdapt::ResizeWindow(short width, short height) {
	SMALL_RECT sr{ 0 };
	SetConsoleWindowInfo(hConsoleOutput, TRUE, &sr);
	COORD bufferSize;
	bufferSize.X = width;
	bufferSize.Y = height;
	SetConsoleScreenBufferSize(hConsoleOutput, bufferSize);

	GetCSBI();
	//Cant oversize the console
	width = min((SHORT)width, this->CSBI.dwMaximumWindowSize.X);
	height = min((SHORT)height, this->CSBI.dwMaximumWindowSize.Y);
	sr.Top = sr.Left = 0;
	sr.Right = width - 1;
	sr.Bottom = height - 1;
	SetConsoleWindowInfo(hConsoleOutput, TRUE, &sr);

	//Need to know the width for string drawing
	this->CurrentConsoleWidth = width;
}

void ConAdapt::SetTitle(string title) {
	THROW_IF_CONSOLE_ERROR(SetConsoleTitleA(title.c_str()));
}

void ConAdapt::DrawString( short x, short y, string const& str, WORD colour) {
	DrawString(x, y, str, vector<WORD>(str.size(), colour));
}

void ConAdapt::DrawString(short x, short y, string const& str, vector<WORD> const& colours) {
	COORD loc{ SHORT(x), SHORT(y) };
	DWORD nCharsWritten;
	DWORD nToWrite = (DWORD)min(str.size(), std::size_t(CurrentConsoleWidth - x));

	THROW_IF_CONSOLE_ERROR(WriteConsoleOutputCharacterA(hConsoleOutput, str.c_str(), nToWrite, loc, &nCharsWritten));
	THROW_IF_CONSOLE_ERROR(WriteConsoleOutputAttribute(hConsoleOutput, colours.data(), nToWrite, loc, &nCharsWritten));
}

void ConAdapt::WriteString(string const& str) {
	DWORD nCharsWritten;
	DWORD nToWrite = str.size();
	THROW_IF_CONSOLE_ERROR(WriteConsoleA(hConsoleOutput, str.c_str(), nToWrite, &nCharsWritten, NULL));
}

int ConAdapt::GetEvents(vector<INPUT_RECORD>& EventRecords) {
	DWORD numEvents;
	THROW_IF_CONSOLE_ERROR(ReadConsoleInput(hConsoleInput, EventRecords.data(), (DWORD)EventRecords.size(), &numEvents));
	return numEvents;
}

void ConAdapt::GetCSBI() {
	THROW_IF_CONSOLE_ERROR(GetConsoleScreenBufferInfo(hConsoleOutput, &this->CSBI));
}

void ConAdapt::GetCCI() {
	THROW_IF_CONSOLE_ERROR(GetConsoleCursorInfo(hConsoleOutput, &this->CCI));
}
void ConAdapt::GetMode() {
	GetConsoleMode(hConsoleInput, &this->ConsoleMode);
}
void ConAdapt::SetMode() {
	SetConsoleMode(hConsoleInput, this->ConsoleMode);
}
#pragma endregion
